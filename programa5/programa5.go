package main

import "fmt"
import (
    "github.com/dghubble/go-twitter/twitter"
    "golang.org/x/oauth2"
    "golang.org/x/oauth2/clientcredentials"    
	"os"
	"strings"
	"time"
	"math/rand"
)

func main() {
	//añadir las credenciales
	config := &clientcredentials.Config{
		ClientID:     "S0AENDlf3pPFqqtMMBGrDYB8z",
		ClientSecret: "t20hmQFuJ0eb6KmWcAAxaSoh3PmwjLjmii0Ttrzz3oGksSXXxv",
		TokenURL:     "https://api.twitter.com/oauth2/token",
	}
	
	httpClient := config.Client(oauth2.NoContext)
	
	client := twitter.NewClient(httpClient)
	
	searchTweetParams := &twitter.SearchTweetParams{
		Query:     "#sorteo exclude:retweets",
		TweetMode: "extended",
		Count:     100,
		//Filter:    "filter:-retweets",
	}
	search, _, _ := client.Search.Tweets(searchTweetParams)

	//crea el archivo en el directorio donde se encuentra el programa1.go
	f, err := os.Create("programa5.csv")
	if err != nil {
        fmt.Println(err)
        return
	}

	var participantes []string
	
	//ciclo donde guarda en el archivo lo que pide el profe
	for _, tweet := range search.Statuses{
		//date, _ := time.Parse(time.RubyDate, tweet.CreatedAt)
		usuario := tweet.User.Name
		//month := date.Month()
		//hour := strconv.Itoa(date.Day())+"/"+strconv.Itoa(int(month))+"/"+strconv.Itoa(date.Year())
		//fechafinal := strconv.Itoa(date.Hour())+":"+strconv.Itoa(date.Minute())+":"+strconv.Itoa(date.Second())
		textTwet := tweet.FullText
		//filtros para distinguir mejor el formato que pide el profe
		textTwet = strings.ReplaceAll(textTwet, "\n", "")
		textTwet = strings.ReplaceAll(textTwet, ";", ",")
		textTwet = strings.ReplaceAll(textTwet, "\"", "'")
		linea := usuario+";"+textTwet
		participantes = append(participantes, usuario)
		//imprime en pantalla el formato pedido por el profe
		//fmt.Println(linea)
		_, err := f.WriteString(linea+"\n")
		if err != nil {
			fmt.Println(err)
			f.Close()
			return
		}
	}
	
	rand.Seed(time.Now().UnixNano())
	ganador := rand.Intn(100)
	suplente := rand.Intn(100)
	
	fmt.Println("El ganador del sorteo es ",participantes[ganador])
	fmt.Println("El suplente del sorteo es ",participantes[suplente])
	
    err = f.Close()
    if err != nil {
        fmt.Println(err)
        return
    }

}