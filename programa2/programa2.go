package main
	
import (
	"math/rand"
	"strings"
    "bufio"
    "fmt"
    "os"
)

//estructura para partir las lineas de los archivos
type tweet struct {
    user string
    hour  string
    date  string
    text  string
}

//algoritmo de quicksort
func quicksort(a []tweet) []tweet {
    if len(a) < 2 {
        return a
    }
      
    left, right := 0, len(a)-1
      
    pivot := rand.Int() % len(a)
      
    a[pivot], a[right] = a[right], a[pivot]
      
    for i, _ := range a {
        if a[i].user < a[right].user {
            a[left], a[i] = a[i], a[left]
            left++
        }
    }
      
    a[left], a[right] = a[right], a[left]
      
    quicksort(a[:left])
    quicksort(a[left+1:])
      
    return a
}

func main(){
	var array []tweet

	//poner la ubicacion donde se encuentra el archivo generado en programa1
	file, err := os.Open("C:/Users/usuario/go/src/estruc/programa1/programa1.csv")
    if err != nil {
        fmt.Println(err)
        return
    }
    defer file.Close()

	//se lee por linea el archivo
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
		partido := strings.Split(scanner.Text(), ";")
		tweet := tweet{partido[0],partido[1],partido[2],partido[3]}
		array = append(array, tweet)
	}

    if err := scanner.Err(); err != nil {
        fmt.Println(err)
        return
	}

	//se ordena con el algoritmo de quicksort
	ordenado := quicksort(array)
	
	//se crea el nuevo archivo en el directorio de programa2
	f, err := os.Create("programa2.csv")
	if err != nil {
        fmt.Println(err)
        return
	}

	//se guarda en el nuevo archivo los tweets ordenados por usuario
	for _, tweet := range ordenado{
		linea := tweet.user+";"+tweet.hour+";"+tweet.date+";"+tweet.text	
		_, err := f.WriteString(linea+"\n")
		if err != nil {
			fmt.Println(err)
			f.Close()
			return
		}
		fmt.Println(linea)
	}
    err = f.Close()
    if err != nil {
        fmt.Println(err)
        return
    }
}
