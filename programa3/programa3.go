package main
	
import (
	"strings"
    "bufio"
    "fmt"
    "os"
)

//estructura para partir las lineas de los archivos
type tweet struct {
    user string
    hour  string
    date  string
    text  string
}

func main(){
	var array []tweet

	//poner la ubicacion donde se encuentra el archivo generado en programa2
	file, err := os.Open("C:/Users/usuario/go/src/estruc/programa2/programa2.csv")
    if err != nil {
        fmt.Println(err)
        return
    }
    defer file.Close()

	//se lee por linea el archivo
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
		partido := strings.Split(scanner.Text(), ";")
		tweet := tweet{partido[0],partido[1],partido[2],partido[3]}
		array = append(array, tweet)
	}

    if err := scanner.Err(); err != nil {
        fmt.Println(err)
        return
	}
	cont:=1
	//se cuentan cuantas veces twiteo un usuario sobre billetaje electronico
	for i:=0;i<len(array);i++{
		if i == len(array) - 1{
			if array[i].user != array[i-1].user{
				fmt.Println(array[i].user," twiteo ",cont," veces sobre billetaje electronico")
			}
		}else if array[i].user == array[i+1].user{
			cont++
		}else{
			fmt.Println(array[i].user," twiteo ",cont," veces sobre billetaje electronico")
			cont = 1
		}
	}
    
}
